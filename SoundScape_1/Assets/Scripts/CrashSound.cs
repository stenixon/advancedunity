﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashSound : MonoBehaviour
{
    public AudioClip crashSoft;
    public AudioClip crashHard;

    private AudioSource source;
    private float lowPitchRange = .75f;
    private float highPitchRange = 1.5f;
    private float velToVol = 0.1f;
    private float velocityClipSplit = 10f;


    void Awake() {
        source = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision coll){
        
        source.pitch = Random.Range(lowPitchRange, highPitchRange);
        float hitVol = coll.relativeVelocity.magnitude * velToVol;
        if (coll.relativeVelocity.magnitude < velocityClipSplit) source.PlayOneShot(crashSoft, hitVol);
        else source.PlayOneShot(crashHard, hitVol);
    }
}
